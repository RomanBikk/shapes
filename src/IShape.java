public interface IShape {
    double area();
}
