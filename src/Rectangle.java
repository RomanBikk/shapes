public class Rectangle implements IShape {
    private double weight;
    private double height;

    public Rectangle(double weight, double height) {
        this.weight = weight;
        this.height = height;
    }

    @Override
    public double area() {
        return this.height*this.weight;
    }
}
