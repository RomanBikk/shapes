import java.util.Arrays;

public class Runner {
    public static void main(String[] args) {
        IShape[] shapes = new IShape[2];
        shapes[0] = new Rectangle(10, 25);
        shapes[1] = new Circle(10.0);
        Arrays.stream(shapes).forEach(IShape -> System.out.println(IShape.area()));

    }
}
